﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RunnerRegister.Startup))]
namespace RunnerRegister
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
