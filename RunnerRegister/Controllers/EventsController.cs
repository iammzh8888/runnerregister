﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using PagedList;
using RunnerRegister.Models;

namespace RunnerRegister.Controllers
{
    public class EventsController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        // GET: Events
        public ActionResult Index(string sortDir, string searchInput, string currentFilter, int? page, string sortOrder = "")
        {
            if (searchInput != null)
                page = 1;
            else
                searchInput = currentFilter;

            ViewBag.CurrentFilter = searchInput;
            ViewBag.sortOrder = sortOrder;
            ViewBag.sortDir = sortDir;

            var events = db.Events.AsQueryable();
            int pageSize = 5;
            int pageNumber = page ?? 1;
            string para = Request.QueryString["page"];
            if (string.IsNullOrEmpty(para))
            {
                events = events.OrderBy(e => e.Name);
                var data1 = events.ToPagedList(pageNumber, pageSize);

                return this.View(data1);
            }
            
            if (!string.IsNullOrEmpty(searchInput))
                events = events.Where(e => e.Name.ToLower().Contains(searchInput.ToLower()));

            switch (sortOrder.ToLower())
            {
                case "name":
                    if (sortDir.ToLower() == "desc")
                        events = events.OrderByDescending(e => e.Name);
                    else
                        events = events.OrderBy(e => e.Name);
                    break;
                case "eventdate":
                    if (sortDir.ToLower() == "desc")
                        events = events.OrderByDescending(e => e.EventDate);
                    else
                        events = events.OrderBy(e => e.EventDate);
                    break;
                case "eventstatus":
                    if (sortDir.ToLower() == "desc")
                        events = events.OrderByDescending(e => e.IsClosed);
                    else
                        events = events.OrderBy(e => e.IsClosed);
                    break;
                default:
                    events = events.OrderBy(e => e.Name);
                    break;
            }

            var data = events.ToPagedList(pageNumber, pageSize);
            return this.PartialView("_EventView", data);
        }
        public ActionResult Search(string searchInput, int? page, string currentFilter)
        {
            if (searchInput != null)
                page = 1;
            else
                searchInput = currentFilter;

            ViewBag.CurrentFilter = searchInput;

            var result = db.Events.AsQueryable(); ;
            if (!string.IsNullOrEmpty(searchInput))
                result = result.Where(e => e.Name.ToLower().Contains(searchInput.ToLower()));

            result = result.OrderBy(e => e.Name);

            int pageSize = 5;
            int pageNumber = page ?? 1;

            var data = result.ToPagedList(pageNumber, pageSize);
            return this.PartialView("_EventView", data);
        }

        // GET: Events/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var @event = db.Events.Find(id);
            if (@event == null) return HttpNotFound();
            return View(@event);
        }

        // GET: Events/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Events/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,EventDate,IsClosed")] Event @event)
        {
            if (ModelState.IsValid)
            {
                db.Events.Add(@event);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(@event);
        }

        // GET: Events/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var @event = db.Events.Find(id);
            if (@event == null) return HttpNotFound();
            return View(@event);
        }

        // POST: Events/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,EventDate,IsClosed")] Event @event)
        {
            if (ModelState.IsValid)
            {
                db.Events.AddOrUpdate(@event);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(@event);
        }

        // GET: Events/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var @event = db.Events.Find(id);
            if (@event == null) return HttpNotFound();
            return View(@event);
        }

        // POST: Events/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var @event = db.Events.Find(id);
            db.Events.Remove(@event);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}