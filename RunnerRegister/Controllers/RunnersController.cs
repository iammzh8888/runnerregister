﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.UI;
using AutoMapper;
using PagedList;
using RunnerRegister.Models;

namespace RunnerRegister.Controllers
{
    public class RunnersController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        // GET: Runners
        public ActionResult Index(string sortDir, string searchInput, string currentFilter, int? page, string sortOrder = "")
        {
            if (searchInput != null)
                page = 1;
            else
                searchInput = currentFilter;

            ViewBag.CurrentFilter = searchInput;
            ViewBag.sortOrder = sortOrder;
            ViewBag.sortDir = sortDir;

            var runners = db.Runners.AsQueryable().Include(r => r.Event).Include(r => r.Gender).Include(r => r.Country);
            int pageSize = 5;
            int pageNumber = page ?? 1;
            string para = Request.QueryString["page"];
            if (string.IsNullOrEmpty(para))
            {
                Mapper.CreateMap<Runner, RunnerViewModel>();
                var runnerVms1 = Mapper.Map<List<Runner>, IEnumerable<RunnerViewModel>>(runners.ToList());
                var data1 = runnerVms1.ToPagedList(pageNumber, pageSize);

                return this.View(data1);
            }

            if (!string.IsNullOrEmpty(searchInput))
                runners = runners.Where(r => r.FirstName.ToLower().Contains(searchInput.ToLower()) || r.LastName.ToLower().Contains(searchInput.ToLower()));

            switch (sortOrder.ToLower())
            {
                case "name":
                    if (sortDir.ToLower() == "desc")
                        runners = runners.OrderByDescending(r => r.FirstName + r.LastName);
                    else
                        runners = runners.OrderBy(r => r.FirstName + r.LastName);
                    break;
                case "gender":
                    if (sortDir.ToLower() == "desc")
                        runners = runners.OrderByDescending(r => r.Gender.GenderType);
                    else
                        runners = runners.OrderBy(r => r.Gender.GenderType);
                    break;
                case "email":
                    if (sortDir.ToLower() == "desc")
                        runners = runners.OrderByDescending(r => r.Email);
                    else
                        runners = runners.OrderBy(r => r.Email);
                    break;
                case "telephone":
                    if (sortDir.ToLower() == "desc")
                        runners = runners.OrderByDescending(r => r.Telephone);
                    else
                        runners = runners.OrderBy(r => r.Telephone);
                    break;
                case "eventname":
                    if (sortDir.ToLower() == "desc")
                        runners = runners.OrderByDescending(r => r.Event.Name);
                    else
                        runners = runners.OrderBy(r => r.Event.Name);
                    break;
                case "eventstatus":
                    if (sortDir.ToLower() == "desc")
                        runners = runners.OrderByDescending(r => r.Event.IsClosed);
                    else
                        runners = runners.OrderBy(r => r.Event.IsClosed);
                    break;
                default:
                    runners = runners.OrderBy(r => r.FirstName + r.LastName);
                    break;
            }
            
            Mapper.CreateMap<Runner, RunnerViewModel>();
            var runnerVms = Mapper.Map<List<Runner>, IEnumerable<RunnerViewModel>>(runners.ToList());
            var data = runnerVms.ToPagedList(pageNumber, pageSize);

            return this.PartialView("_RunnerView", data);
        }
        public ActionResult Search(string searchInput, int? page, string currentFilter)
        {
            if (searchInput != null)
                page = 1;
            else
                searchInput = currentFilter;

            ViewBag.CurrentFilter = searchInput;

            var result = db.Runners.AsQueryable().Include(r => r.Event).Include(r => r.Gender).Include(r => r.Country);
            if (!string.IsNullOrEmpty(searchInput))
                result = result.Where(r => r.FirstName.ToLower().Contains(searchInput.ToLower()) || r.LastName.ToLower().Contains(searchInput.ToLower()));

            int pageSize = 5;
            int pageNumber = page ?? 1;

            Mapper.CreateMap<Runner, RunnerViewModel>();
            var runnerVms = Mapper.Map<List<Runner>, IEnumerable<RunnerViewModel>>(result.ToList());
            var data = runnerVms.ToPagedList(pageNumber, pageSize);
            return this.PartialView("_RunnerView", data);
        }

        // GET: Runners/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var runners = db.Runners.Include(r => r.Event).Include(r => r.Gender).Include(r => r.Country);
            var runner = runners.FirstOrDefault(r => r.Id == id);
            if (runner == null) return HttpNotFound();
            return View(runner);
        }

        // GET: Runners/Create
        public ActionResult Create()
        {
            ViewBag.CountryId = new SelectList(db.Countries, "Id", "CountryName",0);
            ViewBag.EventId = new SelectList(db.Events, "Id", "Name",0);
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "GenderType",0);
            return View();
        }

        // POST: Runners/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include =
                "Id,FirstName,LastName,BirthDate,GenderId,Email,Telephone,Address,State,PostalCode,RegistrationDate,CountryId,ContactName,ContactTelephone,EventId")]
            RunnerViewModel runnerVm)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<RunnerViewModel, Runner>();
                var runner = Mapper.Map<Runner>(runnerVm);
                db.Runners.Add(runner);
                db.SaveChanges();
                ViewBag.CountryId = new SelectList(db.Countries, "Id", "CountryName", runner.CountryId);
                ViewBag.EventId = new SelectList(db.Events, "Id", "Name", runner.EventId);
                ViewBag.GenderId = new SelectList(db.Genders, "Id", "GenderType", runner.GenderId);
                return RedirectToAction("Index");
            }

            return View(runnerVm);
        }

        // GET: Runners/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var runner = db.Runners.Find(id);
            if (runner == null) return HttpNotFound();
            Mapper.CreateMap<Runner, RunnerViewModel>();
            var runnerVm = Mapper.Map<RunnerViewModel>(runner);
            ViewBag.CountryId = new SelectList(db.Countries, "Id", "CountryName", runner.CountryId);
            ViewBag.EventId = new SelectList(db.Events, "Id", "Name", runner.EventId);
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "GenderType", runner.GenderId);
            return View(runnerVm);
        }

        // POST: Runners/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include =
                "Id,FirstName,LastName,BirthDate,GenderId,Email,Telephone,Address,State,PostalCode,RegistrationDate,CountryId,ContactName,ContactTelephone,EventId")]
            RunnerViewModel runnerVm)
        {
            if (ModelState.IsValid)
            {
                Mapper.CreateMap<RunnerViewModel, Runner>();
                var runner = Mapper.Map<Runner>(runnerVm);
                db.Runners.AddOrUpdate(runner);
                db.SaveChanges();
                ViewBag.CountryId = new SelectList(db.Countries, "Id", "CountryName", runner.CountryId);
                ViewBag.GenderId = new SelectList(db.Genders, "Id", "GenderType", runner.GenderId);
                ViewBag.EventId = new SelectList(db.Events, "Id", "Name", runner.EventId);
                return RedirectToAction("Index");
            }

            return View(runnerVm);
        }

        // GET: Runners/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var runner = db.Runners.Find(id);
            if (runner == null) return HttpNotFound();
            return View(runner);
        }

        // POST: Runners/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var runner = db.Runners.Find(id);
            db.Runners.Remove(runner);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}