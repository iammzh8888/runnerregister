﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace RunnerRegister.Hubs
{
    public class DisplayButton : Hub
    {
        public async void Display()
        {
            await Clients.All.SendAsync("DisplayButton");
        }
    }
}