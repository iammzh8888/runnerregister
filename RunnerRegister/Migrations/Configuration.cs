using RunnerRegister.Models;

namespace RunnerRegister.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RunnerRegister.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RunnerRegister.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.Countries.AddOrUpdate(
                  c => c.CountryName,
                  new Country { CountryName = "Albanian" },
                  new Country { CountryName = "Arabic" },
                  new Country { CountryName = "Basque" },
                  new Country { CountryName = "Canada" },
                  new Country { CountryName = "Dutch" },
                  new Country { CountryName = "French" },
                  new Country { CountryName = "Japan" },
                  new Country { CountryName = "Korean" },
                  new Country { CountryName = "United States" }
                );

            context.Genders.AddOrUpdate(
                g => g.GenderType,
                new Gender { GenderType = "FEMALE" },
                new Gender { GenderType = "MALE" },
                new Gender { GenderType = "OTHER" }
            );
        }
    }
}
