﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RunnerRegister.Models
{
    public class Event
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Event Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Event Date")]
        public DateTime EventDate { get; set; }
        [Display(Name = "Is Closed?")]
        public Boolean IsClosed { get; set; }
    }
}