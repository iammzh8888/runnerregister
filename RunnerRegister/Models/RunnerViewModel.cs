﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RunnerRegister.Models
{
    public class RunnerViewModel
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Birth Date")]
        public DateTime BirthDate { get; set; }
        [Display(Name = "Gender")]
        public int GenderId { get; set; }
        public Gender Gender { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        [Display(Name = "Registration Date")]
        public DateTime RegistrationDate { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        [Display(Name = "Contact Name")]
        public string ContactName { get; set; }
        [Display(Name = "Contact Telephone")]
        public string ContactTelephone { get; set; }
        [Display(Name = "Event Name")]
        public int EventId { get; set; }
        public Event Event { get; set; }
    }
}