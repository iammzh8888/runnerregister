﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RunnerRegister.Models
{
    public class Runner
    {
        [Key]
        public int Id { get; set; }
        [StringLength(100)]
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [StringLength(100)]
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Birth Date")]
        public DateTime BirthDate { get; set; }
        [Required]
        [Display(Name = "Gender")]
        public int GenderId { get; set; }
        public Gender Gender { get; set; }
        [StringLength(50)]
        [Required]
        public string Email { get; set; }
        [StringLength(15)]
        [Required]
        public string Telephone { get; set; }
        [StringLength(255)]
        [Required]
        public string Address { get; set; }
        [StringLength(50)]
        [Required]
        public string State { get; set; }
        [StringLength(15)]
        public string PostalCode { get; set; }
        [Display(Name = "Registration Date")]
        [Required]
        public DateTime RegistrationDate { get; set; }
        [Display(Name = "Country")]
        [Required]
        public int CountryId { get; set; }
        public Country Country { get; set; }
        [Display(Name = "Contact Name")]
        [Required]
        public string ContactName { get; set; }
        [Display(Name = "Contact Telephone")]
        [Required]
        public string ContactTelephone { get; set; }
        [Display(Name = "Event Name")]
        [Required]
        public int EventId { get; set; }
        public Event Event { get; set; }
    }
}